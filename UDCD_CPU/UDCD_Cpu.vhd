----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:47:41 01/16/2019 
-- Design Name: 
-- Module Name:    UDCD_Cpu - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UDCD_Cpu is
    Port ( Clk 		: in  STD_LOGIC;
	 
			  HSWAPEN	: in   STD_LOGIC;
			  
           DBG1 		: out  STD_LOGIC;
           DBG2 		: out  STD_LOGIC;
           DBG3 		: out  STD_LOGIC;
           DBG4 		: out  STD_LOGIC;
			  
			  LED_GREEN	: out STD_LOGIC;
           LED_RED 	: out STD_LOGIC;
			  
           TEMP_SCK 	: out STD_LOGIC;
           TEMP_MOSI : out STD_LOGIC;
           TEMP_MISO : in  STD_LOGIC;
           TEMP_CS 	: out STD_LOGIC;
			  
           CFG_SCK	: out STD_LOGIC;
           CFG_MISO0 : in  STD_LOGIC;
           CFG_MISO1 : in  STD_LOGIC;
           CFG_MISO2 : in  STD_LOGIC;
           CFG_MISO3 : in  STD_LOGIC;
           CFG_SCO_B	: out STD_LOGIC;
			  
			  DIN_SCK	: out STD_LOGIC_VECTOR(4 downto 0);
			  DIN_MOSI	: out STD_LOGIC_VECTOR(4 downto 0);
			  DIN_MISO	: in  STD_LOGIC_VECTOR(4 downto 0);
			  DIN_CS1	: out STD_LOGIC_VECTOR(4 downto 0);
			  DIN_CS2	: out STD_LOGIC_VECTOR(4 downto 0);
			  DIN_RESET	: out STD_LOGIC_VECTOR(4 downto 0);
			  DIN_SEL0	: out STD_LOGIC_VECTOR(4 downto 0);
			  DIN_SEL1	: out STD_LOGIC_VECTOR(4 downto 0);
			  
			  DOUT_PP_EN: out STD_LOGIC_VECTOR(7 downto 0);
			  DOUT_OD_EN: out STD_LOGIC_VECTOR(7 downto 0);

           CPU_SCK 	: in  STD_LOGIC;
           CPU_MOSI 	: in  STD_LOGIC;
           CPU_MISO 	: out STD_LOGIC;
           CPU_CS 	: in	STD_LOGIC;
			  CPU_GPIO	: out	STD_LOGIC);
end UDCD_Cpu;

architecture Behavioral of UDCD_Cpu is

begin


end Behavioral;

