----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:04:36 01/16/2019 
-- Design Name: 
-- Module Name:    UDCD_Pwr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity UDCD_Pwr is
    Port ( Clk 			: in	STD_LOGIC;

			  HSWAPEN		: in  STD_LOGIC;

			  LED_GREEN		: out STD_LOGIC;
           LED_RED 		: out STD_LOGIC;
	 
           CFG_SCK		: out STD_LOGIC;
           CFG_MISO0 	: in  STD_LOGIC;
           CFG_MISO1 	: in  STD_LOGIC;
           CFG_MISO2 	: in  STD_LOGIC;
           CFG_MISO3 	: in  STD_LOGIC;
           CFG_SCO_B		: out STD_LOGIC;

           CPU_MISO 		: out STD_LOGIC;
           CPU_MOSI 		: in  STD_LOGIC;
           CPU_SCK 		: in  STD_LOGIC;
           CPU_CS0 		: in  STD_LOGIC;
           CPU_GPIO 		: in  STD_LOGIC;

			  ADC_AC_SCK	: out	STD_LOGIC_VECTOR(4 downto 0);
			  ADC_AC_DOUT	: in	STD_LOGIC_VECTOR(4 downto 0);
			  ADC_AC_CHSEL0: out	STD_LOGIC_VECTOR(4 downto 0);
			  ADC_AC_CHSEL1: out	STD_LOGIC_VECTOR(4 downto 0);
			  
			  ADC_IND_SCK	: out	STD_LOGIC_VECTOR(1 downto 0);
			  ADC_IND_DOUT	: in	STD_LOGIC_VECTOR(1 downto 0);
			  ADC_IND_CHSEL0: out	STD_LOGIC_VECTOR(1 downto 0);
			  ADC_IND_CHSEL1: out	STD_LOGIC_VECTOR(1 downto 0);
			  
			  ADC_SHUNT_SCK	: out	STD_LOGIC_VECTOR(3 downto 0);
			  ADC_SHUNT_DOUT	: in	STD_LOGIC_VECTOR(3 downto 0);
			  ADC_SHUNT_CHSEL0: out	STD_LOGIC_VECTOR(3 downto 0);
			  ADC_SHUNT_CHSEL1: out	STD_LOGIC_VECTOR(3 downto 0);
			  
			  ADC_TRANS_SCK	: out	STD_LOGIC_VECTOR(5 downto 0);
			  ADC_TRANS_DOUT	: in	STD_LOGIC_VECTOR(5 downto 0);
			  ADC_TRANS_CHSEL0: out	STD_LOGIC_VECTOR(5 downto 0);
			  ADC_TRANS_CHSEL1: out	STD_LOGIC_VECTOR(5 downto 0)
			  
	);
end UDCD_Pwr;

architecture Behavioral of UDCD_Pwr is

begin


end Behavioral;

